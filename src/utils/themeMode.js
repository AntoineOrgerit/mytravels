/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import storage from 'local-storage-fallback';

/**
 * Determines if the default theme mode should be dark or light mode on app load.
 * 
 * @returns `dark` if dark mode should be used, `light` otherwise
 */
function getInitialThemeMode() {
    const localTheme = storage.getItem("themeMode");
    const mediaPreference = (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) ? "dark" : "light";
    return !localTheme ? mediaPreference : localTheme;
}

export { getInitialThemeMode };