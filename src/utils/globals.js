/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

/**
 * Imports all images contained in a repository.
 * 
 * @param {Object} repository the repository from where to import the images
 * @returns {Array} the static path of the images
 */
function importAllImages(repository) {
    if (repository === undefined) {
        throw new Error("Parameter `repository` is undefined for images global import of function `importAllImages`.");
    }
    return repository.keys().map(repository);
}

export { importAllImages };