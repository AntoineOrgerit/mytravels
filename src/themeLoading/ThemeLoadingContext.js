/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React, { createContext, useState } from "react";
import PropTypes from "prop-types";

export const ThemeLoadingContext = createContext({});

export const ThemeLoadingProvider = props => {
    const {
        loading: initialLoading,
        children
    } = props;

    const [loading, setLoading] = useState(initialLoading);

    const loadingContext = {
        loading,
        setLoading
    };

    return <ThemeLoadingContext.Provider value={loadingContext}>{children}</ThemeLoadingContext.Provider>;
};

export const { ThemeLoadingConsumer } = ThemeLoadingContext;

ThemeLoadingProvider.propTypes = {
    loading: PropTypes.bool
};

ThemeLoadingProvider.defaultProps = {
    loading: true
};