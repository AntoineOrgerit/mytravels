/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React, { useContext, useEffect } from 'react';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

import { ThemeLoadingContext } from './ThemeLoadingContext';

import "./ThemeLoader.css";

/**
 * Global loader of themes.
 * 
 * @author Antoine Orgerit
 * @version 1.0
 */
export default function ThemeLoader() {
    const antIcon = <LoadingOutlined style={{ fontSize: "20vw" }} spin />;
    const { loading, setLoading } = useContext(ThemeLoadingContext);
    const [isDisplayed, setIsDisplayed] = React.useState(false);

    useEffect(_ => {
        if (loading) {
            setIsDisplayed(true);
            setTimeout(() => {
                setIsDisplayed(false);
                setLoading(false);
            }, 400);
        }
    }, [loading, setLoading]);

    if (isDisplayed) {
        return <Spin id="theme-loader" indicator={antIcon} />;
    } else {
        return null;
    }
}