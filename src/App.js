/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Layout } from 'antd';

import NavBar from './navbar/NavBar';
import Footer from './footer/Footer';
import NoMatch from './noMatch/NoMatch';
import Home from './homepage/Home';

import "./App.css";

const Content = Layout.Content;

class App extends React.Component {
  render() {
    return (
      <Router>
        <Layout id="global-container">
          <NavBar />
          <Content>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route path="*">
                <NoMatch />
              </Route>
            </Switch>
          </Content>
          <Footer />
        </Layout>
      </Router>
    );
  }
}

export default App;
