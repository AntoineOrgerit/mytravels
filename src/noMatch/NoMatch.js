/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { Typography, Button } from 'antd';

import "./NoMatch.css";

const { Title, Paragraph } = Typography;

/**
 * Component used as a 404 content.
 * 
 * @author Antoine Orgerit
 * @version 1.0
 */
export default function NoMatch() {
    return <div id="no-match-container">
        <Title level={3}>404</Title>
        <Paragraph>This page does not exist.</Paragraph>
        <Link to="/"><Button type="primary">Back to home page</Button></Link>
    </div>;
}