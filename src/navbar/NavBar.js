/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { Menu, Layout, Typography } from 'antd';
import Icon, { MenuOutlined } from '@ant-design/icons';

import ThemeSwitcher from '../themeSwitcher/ThemeSwitcher';
import MobileThemeSwitchMenuItem from './MobileThemeSwitchMenuItem';

import {ReactComponent as LogoSVG} from './resources/img/logo.svg';

import './NavBar.css';

const { Header } = Layout;
const { Title } = Typography;

/**
 * Navigation bar used for wide screen resolutions.
 * 
 * @author Antoine Orgerit
 * @version 1.1
 */
export default class NavBar extends React.Component {
    render() {
        const items = [
            <Menu.Item key="world_map"><Link to="/world-map">WORLD MAP</Link></Menu.Item>,
            <Menu.Item key="latest_destination"><Link to="/lastest-destination">LATEST DESTINATION</Link></Menu.Item>,
            <Menu.Item key="all_destinations"><Link to="/destinations">ALL DESTINATIONS</Link></Menu.Item>
        ];

        return (
            <Header className="navbar-container">
                <Menu mode="horizontal" selectable={false} forceSubMenuRender={true}>
                    <Menu.Item className="logo">
                        <Link to="/"><Title><Icon component={LogoSVG} /><span className="hidden-title">MyTravels</span></Title></Link>
                    </Menu.Item>
                    {/* inversed order to match the css property float: right */}
                    <Menu.Item disabled className="theme-switcher-item">
                        <ThemeSwitcher type="button" />
                    </Menu.Item>
                    {[...items].reverse()}
                    <Menu.SubMenu className="mobile-menu" icon={<MenuOutlined />}>
                        {items}
                        <MobileThemeSwitchMenuItem />
                    </Menu.SubMenu>
                </Menu>
            </Header>
        )
    }

}