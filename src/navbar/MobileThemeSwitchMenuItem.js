/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React, { useContext } from 'react';
import storage from 'local-storage-fallback';
import { useThemeSwitcher } from "react-css-theme-switcher";
import { Menu } from 'antd';

import ThemeSwitcher from '../themeSwitcher/ThemeSwitcher';
import { ThemeLoadingContext } from '../themeLoading/ThemeLoadingContext';

/**
 * Custom Menu.Item element for the theme switcher in mobile mode to propagate whole zone click of the menu item
 * to change the theme.
 * 
 * @param {Object} props the properties of the component
 * 
 * @author Antoine Orgerit
 * @version 1.0
 */
export default function MobileThemeSwitchMenuItem(props) {
    const { switcher, themes, currentTheme } = useThemeSwitcher();
    const { setLoading } = useContext(ThemeLoadingContext);

    const toggleThemeMode = params => {
        // making sure we do not completely override the default onClick behaviour
        props.onClick(params);
        switcher({ theme: currentTheme === 'light' ? themes.dark : themes.light });
        storage.setItem("themeMode", currentTheme === 'light' ? "dark" : "light");
        setLoading(true);
    };

    // propagating the properties injected by Menu parent
    return <Menu.Item {...props} className="theme-switcher-item" onClick={toggleThemeMode}>
        <ThemeSwitcher type="switch" />
    </Menu.Item>;
}