/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React from 'react';
import { Layout } from 'antd';
import { MailOutlined, LinkedinFilled } from '@ant-design/icons';

import "./Footer.css";

const LayoutFooter = Layout.Footer;

export default function Footer() {
    return <LayoutFooter id="footer">
        &copy; Antoine Orgerit, 2020. All rights reserved. - Contact: <a href="mailto:orgerit.antoine@gmail.com"><MailOutlined /> orgerit.antoine@gmail.com</a> <a href="https://www.linkedin.com/in/antoine-orgerit/"><LinkedinFilled /> Antoine Orgerit</a>
    </LayoutFooter>;
}