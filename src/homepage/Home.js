/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, Typography, Button } from 'antd';

import Slider from '../commons/Slider.js';
import { importAllImages } from '../utils/globals.js';

import "./Home.css";

const { Title, Paragraph } = Typography;

/**
 * Component used as the home page of the website.
 * 
 * @author Antoine Orgerit
 * @version 1.0
 */
export default function Home() {
    const homeSliderImages = importAllImages(require.context('./img', false, /\.(png|jpe?g|svg)$/));

    return <div id="home-container">
        <Slider images={homeSliderImages} />
        <div id="home-highlights">
            <Row gutter={[32, 32]} justify="space-between" align="middle">
                {[1, 2, 3, 4, 5, 6].map(index => (
                    <Col xs={24} sm={24} md={24} lg={12} key={index}>
                        <Link to="/dummy">
                            <Card hoverable /*bodyStyle={{ backgroundImage: `url('${homeSliderImages[2]}')` }}*/ bordered={false} className="home-highlights-container">
                                <div className="home-highlights-content-description">
                                    <Title level={3}>Vietnam & Cambodia</Title>
                                    <Paragraph>14/07/2020 - 28/07/2020</Paragraph>
                                    <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam volutpat accumsan faucibus. Sed congue lectus ut odio feugiat, sed varius lectus scelerisque. Cras nec arcu.</Paragraph>
                                    <Button type="primary">See more</Button>
                                </div>
                                <div className="home-highlights-content-background" style={{ backgroundImage: `url('${homeSliderImages[2]}')` }} />
                            </Card>
                        </Link>
                    </Col>
                ))}
            </Row>
        </div>
    </div>;
}