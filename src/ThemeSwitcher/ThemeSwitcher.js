/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React, { useContext } from "react";
import PropTypes from 'prop-types';
import storage from 'local-storage-fallback';
import { useThemeSwitcher } from "react-css-theme-switcher";
import { Button, Switch } from "antd";
import Icon from "@ant-design/icons";

import { ThemeLoadingContext } from '../themeLoading/ThemeLoadingContext';
import { ReactComponent as SunSVG } from './sun.svg';
import { ReactComponent as MoonSVG } from './moon.svg';

import './ThemeSwitcher.css';

/**
 * Switcher used to change theme if required.
 * 
 * @param {Object} props the properties of the component
 * 
 * @author Antoine Orgerit
 * @version 1.2
 */
export default function ThemeSwitcher(props) {
    const { switcher, themes, currentTheme } = useThemeSwitcher();
    const { setLoading } = useContext(ThemeLoadingContext);

    const toggleThemeMode = _ => {
        switcher({ theme: currentTheme === 'light' ? themes.dark : themes.light });
        storage.setItem("themeMode", currentTheme === 'light' ? "dark" : "light");
        setLoading(true);
    };

    switch (props.type) {
        case "button":
            return <Button className="theme-switcher-btn" type="primary" shape="circle" onClick={toggleThemeMode} icon={currentTheme === "dark" ? <Icon component={SunSVG} /> : <Icon component={MoonSVG} />} />;
        case "switch":
            return <span className="theme-switcher-switch"><Icon component={SunSVG} /><Switch onClick={toggleThemeMode} checked={currentTheme === "dark"} /><Icon component={MoonSVG} /></span>;
        default:
            throw new Error("ThemeSwitcher `type` property was assigned the following unsupported value: `" + props.type + "`");
    }
}

ThemeSwitcher.propTypes = {
    type: PropTypes.string
};

ThemeSwitcher.defaultProps = {
    type: "button"
};