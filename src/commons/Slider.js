/**
 * Copyright 2020 Antoine Orgerit. All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import React, { createRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Carousel } from 'antd';
import { LeftOutlined, RightOutlined, FullscreenExitOutlined, FullscreenOutlined } from '@ant-design/icons';

import './Slider.css';

/**
 * Displays a set of content as a slider.
 * 
 * @param {Object} props the property of the component
 * 
 * @author Antoine Orgerit
 * @version 1.0
 */
export default function Slider(props) {
    let slider = createRef();
    const [isFullscreen, setIsFullScreen] = useState(false);

    let defaultStyle = {
        backgroundSize: "cover",
        backgroundPosition: "center center",
        backgroundRepeat: "no-repeat"
    };

    const next = _ => {
        slider.next();
    };

    const previous = _ => {
        slider.prev();
    };

    const toggleFullscreen = _ => {
        setIsFullScreen(!isFullscreen);
    };

    return <div className={"slider" + (isFullscreen ? " slider-fullscreen" : "")}>
        <div className="slider-actions-container">
            <LeftOutlined className="slider-action slider-action-left" onClick={previous} />
            <Carousel dotPosition="bottom" autoplay autoplaySpeed={6000} pauseOnFocus swipeToSlide accessibility draggable ref={c => (slider = c)}>
                {props.images.map((imgSrc, index) => {
                    return <div className="slider-item" key={index}>
                        <div style={{ ...defaultStyle, backgroundImage: `url(${imgSrc})` }}>
                        </div>
                    </div>;
                })}
            </Carousel>
            <RightOutlined className="slider-action slider-action-right" onClick={next} />
            {isFullscreen ? <FullscreenExitOutlined className="slider-action slider-action-fullscreen" onClick={toggleFullscreen} /> : <FullscreenOutlined className="slider-action slider-action-fullscreen" onClick={toggleFullscreen} />}
        </div>
    </div>;
}

Slider.propTypes = {
    images: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
};